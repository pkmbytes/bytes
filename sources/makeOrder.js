var makeOrderPage = new Class({
	initialize : function()
	{
		this.orderDetails = JSON.parse(localStorage.TransactionDetails);
		this.items = details.productDetails;
		this.renderMakeOrderPage();
	},
	renderMakeOrderPage : function()
	{
		var _self = this;
		var htmlElem = [];
		for (var key in _self.items) 
		{
		  if (_self.items.hasOwnProperty(key)) 
		  {
			  htmlElem.push('<option value="' + key + '">' + _self.items[key].productName + '</option>');
		  }
		}
		$(".itemList").append(htmlElem.join(""));
		_self.toggleContainer();
	},
	toggleContainer : function()
	{
		if($("#itempadContainer").is(":visible") || $("#numpadContainer").is(":visible") )
		{
			$("#displayContainer").addClass("hide");
			$("#itempadContainer").addClass("hide");
			$("#itemBillPageBtnCont").addClass("hide");
			$("#makeOrderTemplate").removeClass("hide");
		}
	}
});