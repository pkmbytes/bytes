var productDetailsList = new getDetailsAboutProduct(details);
var orderHistoryPage = new Class({
	initialize : function()
	{
		this.orderDetails = JSON.parse(localStorage.TransactionDetails); 
		this.renderOrderHistoryPage();
	},
	renderOrderHistoryPage : function()
	{
		_self = this;
		_self.toggleContainer();
		for(var iterate = 0; iterate < _self.orderDetails.length; iterate++)
		{
			var ordrDetl = [];
			ordrDetl.push("<div class='orderListParent'>");
				ordrDetl.push("<div class='orderListImg'></div>");
				ordrDetl.push("<div class='orderListDetails'>");
					ordrDetl.push("<div class='orderTransactionId'>" + _self.orderDetails[iterate].orderid + "</div>");
					ordrDetl.push("<div class='orderTotalPrice'>" + _self.orderDetails[iterate].totalPrice + "</div>");
				ordrDetl.push("</div>");
				ordrDetl.push("<div class='orderInfo hide'>");
				for(var orderIterate = 0; orderIterate < _self.orderDetails[iterate].orders.length; orderIterate++)
				{
					ordrDetl.push("<div class='productName'>" + productDetailsList.getProductName(_self.orderDetails[iterate].orders[orderIterate].productId) + "</div>")
					ordrDetl.push("<div class='proudctQuantity'>" + _self.orderDetails[iterate].orders[orderIterate].quantity + "</div>");
				}
			ordrDetl.push("</div>");
			ordrDetl.push("</div>");
			$("#orderHistoryPageTemplate").append(ordrDetl.join(""));
		}
		_self.registerEvents();
	},
	registerEvents : function()
	{
		$(".orderListParent").off("click").on("click", function(){
			
			if(!$(".orderInfo").is(":visible"))
			{
				$(this).find(".orderInfo").removeClass("hide");
			}
			else
			{
				$(this).find(".orderInfo").addClass("hide");
			}
		});
	},
	toggleContainer : function()
	{
		if($("#itempadContainer").is(":visible") || $("#numpadContainer").is(":visible") )
		{
			$("#displayContainer").addClass("hide");
			$("#itempadContainer").addClass("hide");
			$("#itemBillPageBtnCont").addClass("hide");
			$("#orderHistoryPageTemplate").removeClass("hide");
		}
	}
});