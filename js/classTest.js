var Animal = new Class({
    initialize: function (name, age) {
        this.name = name;
        this.age = age;
    },
    eat: function () {
        _self = this;
        alert(_self.name + ' is eating now.');
        _self.registerEvents();
    },
    registerEvents : function()
    {
        window.alert("Register Events function called");
    }
});