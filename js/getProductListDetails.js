var getDetailsAboutProduct = new Class({
	initialize : function(details)
	{
		this.details = details;
	},
	getProductDetails : function(productID)
    {
        _this = this;
        _this.productDetails = _this.details.productDetails;
        var productDetails = {
        		"name" : (_this.productDetails ? _this.productDetails[productID].productName : ""),
        		"price" : (_this.productDetails ? _this.productDetails[productID].productPrice: "")
        };
        return productDetails;
    },
	getProductName : function(productId)
	{
		_this = this;
		return _this.details.productDetails[productId].productName;
	},
    getProductPrice : function(productId)
    {
    	_this = this;
		return parseFloat(_this.details.productDetails[productId].productPrice);
    }
});