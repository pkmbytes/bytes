var productDetailsList = new getDetailsAboutProduct(details);
var billingPage = new Class(
{
	initialize : function(data)
	{
		this.productDetails = data.productDetails;
		this.productKeyboardDetails = data.productKeyboardDetails;
		this.curntProduct = {};
		this.crntBillProdt = [];
		this.itemBtnOffset = 1;
		this.maxItemBtnLimit = 6;
		this.transactionDetails = {};
		this.itemBtnLimit = this.maxItemBtnLimit;
		
		this.itemButtonElements(this.itemBtnOffset, this.itemBtnLimit, true);
	},
	itemButtonElements : function(offset, limit, isFromNavigation) 
	{
		var _self = this;
		var itemBtnOffset = offset;
		var itemLimit = limit;
	    var htmlElem = [];
	    for(itemBtnOffset; itemBtnOffset <= itemLimit; itemBtnOffset++)
	    {
	    	if($(".itempad").length)
    		{
	    		$(".itempad").remove();
    		}
	    	var itemBtnProduct = _self.productKeyboardDetails["ITEMBUTTON_"+itemBtnOffset].btnProductID;
	        htmlElem.push('<div class="itempad"><div class="itempadBtn btn btn-primary btn-sm" clickValue= "'+itemBtnProduct+'">' + productDetailsList.getProductName(itemBtnProduct) +'</div></div>');
	    }
//	    if(isFromNavigation)
//    	{
//	    	htmlElem.push('<div class="itempadPrintBtnCont"><div class="itempadBtn printBtn btn btn-primary">Print</div></div>');
//		    htmlElem.push("<div class='itemNavButtonCont'><div class='btn btn-primary btn-sm' id='itemPrevBtn'>Previous</div><div class='btn btn-primary btn-sm' id='itemNextBtn'>Next</div></div>");
//    	}
	    //$(htmlElem.join("")).insertBefore(".itemBtnManipulateCont");
	    $("#itempadContainer").html($(htmlElem.join("")))
	    _self.registerItemBtnEvents();
	},
	registerItemBtnEvents : function()
	{ 
		_self = this;
		$(".itempadBtn").off("click").on("click", function(){
	       $(".productText").text($(this).text());
	       $(".productText").attr("productId" , $(this).attr("clickValue"));
	       _self.toggleContainer();
	    });
		$(".printBtn").off("click").on("click", function(){
			_self.calculateAmount(false);
		});
		$("#itemNextBtn").off("click").on("click", function(){
			_self.itemBtnOffset = _self.itemBtnOffset + _self.itemBtnLimit;
			
			if((_self.itemBtnLimit + _self.maxItemBtnLimit)  > Object.keys(_self.productKeyboardDetails).length)
			{	
				_self.itemBtnLimit = Object.keys(_self.productKeyboardDetails).length;
			}
			else
			{
				_self.itemBtnLimit += _self.maxItemBtnLimit; 
			}
			_self.itemButtonElements(_self.itemBtnOffset, _self.itemBtnLimit, false);
		});
		$("#itemPrevBtn").off("click").on("click", function(){
			_self.itemBtnOffset = _self.itemBtnLimit - _self.itemBtnOffset;
			if(_self.itemBtnOffset < _self.maxItemBtnLimit)
			{
				_self.itemBtnOffset = 1;
			}
			if((_self.itemBtnLimit - _self.maxItemBtnLimit)  <= 0)
			{	
				_self.itemBtnLimit = _self.itemBtnLimit
			}
			else
			{
				_self.itemBtnLimit -= _self.maxItemBtnLimit; 
			}
			_self.itemButtonElements(_self.itemBtnOffset, _self.itemBtnLimit, false);
		});
		$(".mkOrderHistory").off("click").on("click", function(){
			var orderHistoryObj = new orderHistoryPage();
		});
		$(".mkOrder").off("click").on("click", function(){
			var makeOrderObj = new makeOrderPage();
		});
		$("#orderUserQty").off("blur").on("blur", function(){
			var grandTotal = parseFloat($("#orderUserQty").val()) * productDetailsList.getProductPrice($(".itemList").val());
			$("#orderUserGrandTotal").val(grandTotal);
		});
	},
	registerNumpadBtnEvents : function()
	{
		var _self = this;
		var displayPrice = "";
		$(".numpadBtn").off("click").on("click", function(){
			$("#itemQty").val($("#itemQty").val() + $(this).text());
			_self.curntProduct.productId =  $(".productText").attr("productId");
			_self.calculateAmount(_self.curntProduct, true);
			$("#totalPrice").val(_self.calculateAmount(_self.curntProduct, true));
		});
		
		$(".itemsBtn").off("click").on("click", function(){
			_self.toggleContainer();
			$(".productText").text("Product");
		});

		$(".dltBtn").off("click").on("click", function(){
			$("#itemQty").val($("#itemQty").val().slice(0,-1));
			if($("#itemQty").val() != "")
			{
				$("#totalPrice").val(_self.calculateAmount(true));
			}
			else
			{
				$("#totalPrice").val("");
				_self.toggleContainer();
			}
		});
		
		$(".cashBtn").off("click").on("click", function(){
			if($("#itemQty").val() != "" && $("#totalPrice").val() != "")
			{
				_self.crntBillProdt.push(_self.curntProduct);
				$("#itemQty, #totalPrice").val("");
				$(".productText").text("Product");
				_self.curntProduct = {};
				_self.toggleContainer();
			}
		});
	},
	calculateAmount : function(isSingleItem)
	{
		var _self = this;
		var totalPrice = 0;
		if(isSingleItem)
		{
			totalPrice = parseFloat($("#itemQty").val()) * productDetailsList.getProductPrice(_self.curntProduct.productId);
			_self.curntProduct.quantity = parseInt($("#itemQty").val());
			_self.curntProduct.totalPrice = totalPrice;
			return totalPrice;
		}
		else
		{
			for(var iterate = 0; iterate < _self.crntBillProdt.length; iterate++)
			{
				totalPrice += parseFloat(_self.crntBillProdt[iterate].totalPrice);
			}
			_self.createTransactionDetails(totalPrice);
		}
	},
	createTransactionDetails : function(totalPrice)
	{
		_self.transactionDetails.orders = [];
		_self.transactionDetails.orderid = "Trans_"+ moment().format("YYYYMMDDHHMMSS") + "_BS";
		_self.transactionDetails.totalPrice = totalPrice;
		for(iterate = 0; iterate < _self.crntBillProdt.length; iterate++)
		{
			_self.transactionDetails.orders.push(_self.crntBillProdt[iterate]);
		}
		_self.crntBillProdt = [];
		_self.setLocalStorage();
	},
	setLocalStorage : function()
	{
		if(typeof(localStorage) != "undefined")
		{
			if(localStorage.getItem("TransactionDetails") != null)
			{ 
				
				var olderDetails = (JSON.parse(localStorage.getItem("TransactionDetails")));
				localStorage.removeItem("TransactionDetails");
				olderDetails.push(_self.transactionDetails)
				localStorage.setItem("TransactionDetails", JSON.stringify(olderDetails));
			}
			else
			{
				var olderDetails = [];
				olderDetails.push(_self.transactionDetails)
				localStorage.setItem("TransactionDetails", JSON.stringify(olderDetails));
			}
			console.log("Local Storage : " + _self.transactionDetails)
		}
		_self.transactionDetails = {};
		//_self.printLable();
		
	},
	printLable : function()
	{
		for(var iterate = 0; iterate < _self.transactionDetails.length; iterate++)
		{
			console.log("product : " + productDetailsList.getProductName(_self.crntBillProdt[iterate].productId));
			console.log("Quantity: " + _self.crntBillProdt[iterate].quantity);
			console.log("Price: " + productDetailsList.getProductPrice(_self.crntBillProdt[iterate].productId));
		}
		console.log("Total Price : " + totalPrice);
		_self.crntBillProdt = [];
	},
	toggleContainer : function()
	{
		var _self = this;
		if($("#itempadContainer").is(":visible"))
		{
			$("#displayContainer").removeClass("hide");
			$("#itempadContainer").addClass("hide");
			$("#numpadContainer").removeClass("hide");
			$("#itemBillPageBtnCont").addClass("hide");	
			$("#itemNumPadBtnCont").removeClass("hide");
			_self.registerNumpadBtnEvents();
		}
		else
		{
			$("#displayContainer").addClass("hide");
			$("#itempadContainer").removeClass("hide");
			$("#itemBillPageBtnCont").removeClass("hide");
			$("#itemNumPadBtnCont").addClass("hide");
			$("#numpadContainer").addClass("hide");
		}
	}
});